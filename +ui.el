;;; +ui.el --- Description -*- lexical-binding: t; -*-

(setq doom-theme 'doom-one-light
      doom-font (font-spec :family "SF Mono" :size 12)
      doom-variable-pitch-font (font-spec :family "New York" :size 13))
      ;; doom-serif-font (font-spec :family "Menlo"))

;; I'm impatient and still new, go quickly
(setq which-key-idle-delay 0.25)

(setq company-idle-delay nil)
(setq display-line-numbers-type t)

(setq initial-frame-alist '((top . 1) (left . 1) (width . 200) (height . 85)))
(add-to-list 'default-frame-alist '(inhibit-double-buffering . t))

;;; :app everywhere
(after! emacs-everywhere
  ;; Easier to match with a bspwm rule:
  ;;   bspc rule -a 'Emacs:emacs-everywhere' state=floating sticky=on
  (setq emacs-everywhere-frame-name-format "emacs-anywhere")

  ;; The modeline is not useful to me in the popup window. It looks much nicer
  ;; to hide it.
  (remove-hook 'emacs-everywhere-init-hooks #'hide-mode-line-mode)

  ;; Semi-center it over the target window, rather than at the cursor position
  ;; (which could be anywhere).
  (defadvice! center-emacs-everywhere-in-origin-window (frame window-info)
    :override #'emacs-everywhere-set-frame-position
    (cl-destructuring-bind (x y width height)
        (emacs-everywhere-window-geometry window-info)
      (set-frame-position frame
                          (+ x (/ width 2) (- (/ width 2)))
                          (+ y (/ height 2))))))

(run-with-timer 0 3600 'synchronize-theme)
