;;; autoload/personal.el -*- lexical-binding: t; -*-

;;;###autoload
(defun find-in-dotfiles ()
  "Open a file somewhere in ~/.local/share/chezmoi via a fuzzy filename search."
  (interactive)
  (doom-project-find-file (expand-file-name "~/.local/share/chezmoi")))

;;;###autoload
(defun browse-dotfiles ()
  "Browse the files in ~/.local/share/chezmoi."
  (interactive)
  (doom-project-browse (expand-file-name "~/.local/share/chezmoi")))

;; adapted from https://stackoverflow.com/questions/14760567/emacs-auto-load-color-theme-by-time
;;;###autoload
(defun synchronize-theme ()
  (let* ((light-theme 'doom-one-light)
         (dark-theme 'doom-one)
         (start-time-light-theme 6)     ; inclusive
         (end-time-light-theme 17)      ; inclusive
         (hour (string-to-number (substring (current-time-string) 11 13)))
         (next-theme (if (member hour (number-sequence start-time-light-theme end-time-light-theme))
                         light-theme dark-theme)))
    (when (not (equal doom-theme next-theme))
      (setq doom-theme next-theme)
      (load-theme next-theme))))
