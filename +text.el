;;; +text.el --- Description -*- lexical-binding: t; -*-

(after! org
  (setq org-directory (file-truename "~/org")
        org-roam-directory (file-truename "~/org")
        org-agenda-files (append '("~/org") (file-expand-wildcards "~/org/daily/*")))

  (setq org-roam-notes-path (or (getenv "ORG_ROAM_NOTES_PATH") "~/org/notes"))
  (setq org-roam-publish-path (or (getenv "ORG_ROAM_PUBLISH_PATH") "~/hugo/marsposting"))
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-id-link-to-org-use-id t)

  (setq-default org-display-custom-times t)
  (setq org-time-stamp-custom-formats '("<%a %b %e %Y>" . "<%a %b %e %Y %H:%M>"))
  (setq org-todo-keywords '((sequence "TODO(t)" "WAIT(w)" "HOLD(h)" "|" "DONE(d)" "CANCELLED(c)")
                            (type     "PROJ(p)" "ISSUE(i)" "BUG(b)" "REVIEW(r)" "|" "SOLVED(s)")))
  (org-roam-db-autosync-mode)
  org-roam-completion-everywhere t

  (setq org-id-extra-files (org-roam--list-files org-roam-directory)))

(after! org-clock
  ;; Resume clocking task when emacs is restarted
  (org-clock-persistence-insinuate)
  ;; Show lot of clocking history so it's easy to pick items from a list
  (setq org-clock-history-length 23)
  ;; Resume clocking task on clock-in if the clock is open
  (setq org-clock-in-resume t)
  ;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
  (setq org-clock-out-remove-zero-time-clocks t)
  ;; Clock out when moving task to a done state
  (setq org-clock-out-when-done t)
  ;; Save the running clock and all clock history when exiting Emacs, load it on startup
  (setq org-clock-persist t)
  ;; Include current clocking task in clock reports
  (setq org-clock-report-include-clocking-task t))

(after! org-capture
  (setq org-capture-templates
        '(("t" "Todo" entry (file+headline "~/org/refile.org" "Tasks")
           "* TODO %?\n")
          ("d" "Default" entry (file+headline "~/org/refile.org" "Notes")
           "* %?\n%a")
          ("w" "Question" entry (file "~/org/refile.org")
           "* %? :question:\n")
          ;; ("p" "Protocol" entry (file+headline ,(concat org-directory "refile.org") "Inbox")
          ;;  "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
          ;; ("L" "Protocol Link" entry (file+headline ,(concat org-directory "refile.org") "Inbox")
          ;;  "* %? [[%:link][%:description]] \nCaptured on: %U")
          ("c" "Default with context" entry (file "~/org/refile.org")
           "* %T %x\n %?\n"))))

(after! org-roam
  (setq org-roam-dailies-directory "daily/")
  (setq org-roam-dailies-capture-templates
        '(("t" "task" entry
           "* TODO %?"
           :target (file+head+olp "%<%Y_%m_%d>.org" "#+title: %<%Y_%m_%d>\n* Tasks\n\n* Notes\n\n" ("Tasks")))
          ("n" "note" entry
           "* %?"
           :target (file+head+olp "%<%Y_%m_%d>.org" "#+title: %<%Y_%m_%d>\n* Tasks\n\n* Notes\n\n" ("Notes"))))
  (setq org-roam-capture-templates
        '(("d" "default" plain
           "* %?" :target
           (file+head "${slug}.org" "#+title: ${title}\n")
           :unnarrowed t)
          ("b" "book" plain "%?"
           :if-new
           (file+head "books/${slug}.org" "#+title: ${title}\n#+filetags: :book:\n")
           :immediate-finish t
           :unnarrowed t)
          ("p" "person" plain "%?"
           :if-new
           (file+head "people/${slug}.org" "#+title: ${title}\n#+filetags: :person:\n")
           :immediate-finish t
           :unnarrowed t)
          ("r" "project" plain "%?"
           :if-new
           (file+head "projects/${slug}.org" "#+title: ${title}\n#+filetags: :project:\n")
           :immediate-finish t
           :unnarrowed t)
          ("a" "atom" plain "%?"
           :if-new
           (file+head "atoms/${slug}.org" "#+title: ${title}\n#+filetags: :atom:stub:\n")
           :immediate-finish t
           :unnarrowed t)
          ("m" "molecule" plain "%?"
           :if-new
           (file+head "molecules/${slug}.org" "#+title: ${title}\n#+filetags: :molecule:stub:\n")
           :immediate-finish t
           :unnarrowed t)
          ("e" "essay" plain "%?"
           :if-new
           (file+head "essays/${slug}.org" "#+title: ${title}\n#+filetags: :essay:\n")
           :immediate-finish t
           :unnarrowed t))))

(setq org-hugo-base-dir "~/hugo")

(use-package! olivetti
  :init
  (setq-default olivetti-body-width 0.618)
  :commands olivetti-mode)
