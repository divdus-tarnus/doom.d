;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(load! "+keys")
(load! "+lsp")
(load! "+text")
(load! "+ui")
(load! "+rss")
(when (eq (getenv "WORK") 1)
  (load! "+work"))

(setq user-full-name "~divdus-tarnus"
      user-mail-address "divdus-tarnus@pm.me")

(setq evil-split-window-below t
      evil-vsplit-window-right t)

(add-to-list 'auto-mode-alist '("\\.zsh\\'" . sh-mode))

(add-hook 'hoon-mode
          (lambda ()
            (define-key hoon-mode-map (kbd "C-c r") 'hoon-eval-region-in-herb)
            (define-key hoon-mode-map (kbd "C-c b") 'hoon-eval-buffer-in-herb)))
(add-hook 'hoon-mode #'lsp)

(after! elfeed
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/org/elfeed.org")))
