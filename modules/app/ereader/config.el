;;; app/ereader/config.el -*- lexical-binding: t; -*-

(use-package! nov
  :mode ("\\.epub\\'" . nov-mode)
  :hook (nov-mode . mixed-pitch-mode)
  :hook (nov-mode . visual-line-mode)
  :hook (nov-mode . visual-fill-column-mode)
  :config
  (setq nov-text-width t)
  (setq nov-variable-pitch t))

(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "New York"
                                           :height 1.1))
(add-hook 'nov-mode-hook 'my-nov-font-setup)

(use-package! calibre
  :defer t
  :config
  (setq calibredb-root-dir "~/Calibre")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir)))

(use-package! calibredb
  :defer t)
