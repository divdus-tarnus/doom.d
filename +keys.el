;;; +keys.el --- Description -*- lexical-binding: t; -*-

(when IS-MAC (setq mac-command-modifier 'super
                   mac-option-modifier  'meta))

(map! :nv "-" #'dired-jump
      :n "M-h" #'evil-window-left
      :n "M-j" #'evil-window-down
      :n "M-k" #'evil-window-up
      :n "M-l" #'evil-window-right
      :n "gx" #'browse-url-at-point
      :n "C-c a" #'org-agenda

      :leader
      (:prefix "o"
        "c" #'calibredb)
      (:prefix "r"
       "f" #'org-roam-node-find
       "i" #'org-roam-node-insert
       "a" #'org-roam-alias-add
       "t" #'org-roam-tag-add
       "s" #'org-roam-db-sync
       "x" #'org-roam-dailies-capture-today
       (:prefix "j"
        "d" #'org-roam-dailies-goto-today
        "t" #'org-roam-dailies-goto-tomorrow))
      (:prefix "f"
       "t" #'find-in-dotfiles
       "T" #'browse-dotfiles))
       ;; "p" #'find-in-doom-config
       ;; "P" #'browse-doom-config))
